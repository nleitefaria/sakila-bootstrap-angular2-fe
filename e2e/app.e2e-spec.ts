import { SakilaBootstrapAngular2FePage } from './app.po';

describe('sakila-bootstrap-angular2-fe App', () => {
  let page: SakilaBootstrapAngular2FePage;

  beforeEach(() => {
    page = new SakilaBootstrapAngular2FePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
