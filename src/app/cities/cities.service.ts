import { Injectable } from '@angular/core';
import { ICities } from './cities';
import {Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';

@Injectable()
export class CitiesService {

  constructor(private http: Http) { }
  
  getCities() 
  {
		return this.http.get(`http://localhost:8084/poc-spring-mysql-be/cities`)
						.map((res:Response) => <ICities[]>res.json());
  }

}
