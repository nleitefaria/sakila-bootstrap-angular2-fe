import { Component, OnInit } from '@angular/core';
import { CitiesService } from './cities.service';
import { ICities } from './cities';

@Component({
  selector: 'app-cities',
  templateUrl: './cities.component.html',
  styleUrls: ['./cities.component.css']
})
export class CitiesComponent implements OnInit {

  private cities : ICities[];
  title : string;

  ngOnInit() 
  {
	this.title = "Cities";
  }
  
  constructor(citiesService: CitiesService)
  {
	citiesService.getCities().subscribe(data => this.cities = data);	
  }

}
