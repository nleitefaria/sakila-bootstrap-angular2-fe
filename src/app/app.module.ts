import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { CountriesComponent } from './countries/countries.component';
import { CitiesComponent } from './cities/cities.component';

import { CountriesService } from './countries/countries.service';
import { CitiesService } from './cities/cities.service';



@NgModule({
  declarations: [
    AppComponent,
    CountriesComponent,
    CitiesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
	AppRoutingModule
  ],
  providers: [CountriesService, CitiesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
