import { Component, OnInit } from '@angular/core';
import { CountriesService } from './countries.service';
import { ICountries } from './countries';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.css']
})
export class CountriesComponent implements OnInit {

  private countries : ICountries[];
  title : string;

  ngOnInit() 
  {
	this.title = "Countries";
  }
  
  constructor(countriesService: CountriesService)
  {
	countriesService.getCountries().subscribe(data => this.countries = data);	
  }

}
